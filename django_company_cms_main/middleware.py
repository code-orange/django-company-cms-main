from django.urls import reverse


class PingbackMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        response["X-Pingback"] = request.build_absolute_uri(reverse("xml-rpc"))
        return response
