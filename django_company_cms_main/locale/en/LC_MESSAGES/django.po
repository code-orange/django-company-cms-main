# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-07 21:53+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: django_company_cms_main/django_company_cms_main/context_processors.py:83
msgid "Sorry, there was an issue processing the contact data."
msgstr ""

#: django_company_cms_main/django_company_cms_main/context_processors.py:89
msgid "Thank you for your inquiry. We will contact you as soon as possible."
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:12
msgid "Services"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:15
msgid "MSP - Managed Services Provider"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:19
msgid "IT Automation"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:23
msgid "IT Documentation"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:27
msgid "Configuration Management"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:31
msgid "Software Development"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:35
msgid "System Monitoring"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:44
msgid "Communication"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:47
msgid "Internet Availability Check"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:51
msgid "Public WLAN & Guest Access"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:55
msgid "Unified Communications"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:59
msgid "IP-Telephony / VoIP / SIP"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:63
msgid "CloudPBX"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:67
msgid "Hosted Exchange"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:71
msgid "E-Mail Archiving"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:75
msgid "Dolphin Meet"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:79
msgid "Dolphin Team"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:83
msgid "E-Fax"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:87
msgid "SMS-Gateway"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:91
msgid "Email Signature"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:100
msgid "Cloud"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:103
msgid "Cloud / XaaS - Anything as a Service"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:107
msgid "Dedicated Server"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:111
msgid "Virtual Server"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:115
msgid "VDI - Virtual Desktop Infrastructure"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:119
msgid "Webhosting"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:123
msgid "Domains & DNS"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:127
msgid "Fileshare"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:131
msgid "Online Backup"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:135
msgid "Network Management"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:139
msgid "Network Security"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:143
msgid "IoT - Internet of Things"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:147
msgid "IAM - Identity & Access Management"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:156
msgid "About Us"
msgstr ""

#: django_company_cms_main/django_company_cms_main/views.py:170
msgid "Blog"
msgstr ""
