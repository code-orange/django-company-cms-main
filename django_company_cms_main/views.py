from django.urls import reverse
from django.utils.translation import gettext as _

from django_template_landrick.django_template_landrick.views import *


def get_nav(request):
    nav = list()

    nav.append(
        MenuGroup(
            menu_name=_("Services"),
            menu_items=(
                MenuLink(
                    link_name=_("MSP - Managed Services Provider"),
                    link_url=reverse("msp"),
                ),
                MenuLink(
                    link_name=_("IT Automation"),
                    link_url=reverse("msp_it_automation"),
                ),
                MenuLink(
                    link_name=_("IT Documentation"),
                    link_url=reverse("msp_it_documentation"),
                ),
                MenuLink(
                    link_name=_("Configuration Management"),
                    link_url=reverse("msp_configuration_management"),
                ),
                MenuLink(
                    link_name=_("Software Development"),
                    link_url=reverse("msp_software_development"),
                ),
                MenuLink(
                    link_name=_("System Monitoring"),
                    link_url=reverse("msp_system_monitoring"),
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Communication"),
            menu_items=(
                MenuLink(
                    link_name=_("Internet Availability Check"),
                    link_url=reverse("isp_internet_check"),
                ),
                MenuLink(
                    link_name=_("Public WLAN & Guest Access"),
                    link_url=reverse("isp_public_wlan"),
                ),
                MenuLink(
                    link_name=_("Unified Communications"),
                    link_url=reverse("uc"),
                ),
                MenuLink(
                    link_name=_("IP-Telephony / VoIP / SIP"),
                    link_url=reverse("voip"),
                ),
                MenuLink(
                    link_name=_("CloudPBX"),
                    link_url=reverse("voip_cloud_pbx"),
                ),
                MenuLink(
                    link_name=_("Hosted Exchange"),
                    link_url=reverse("uc_hosted_exchange"),
                ),
                MenuLink(
                    link_name=_("E-Mail Archiving"),
                    link_url=reverse("uc_email_archiving"),
                ),
                MenuLink(
                    link_name=_("Dolphin Meet"),
                    link_url=reverse("uc_dolphin_meet"),
                ),
                MenuLink(
                    link_name=_("Dolphin Team"),
                    link_url=reverse("uc_dolphin_team"),
                ),
                MenuLink(
                    link_name=_("E-Fax"),
                    link_url=reverse("uc_e_fax"),
                ),
                MenuLink(
                    link_name=_("SMS-Gateway"),
                    link_url=reverse("uc_sms_gateway"),
                ),
                MenuLink(
                    link_name=_("Email Signature"),
                    link_url=reverse("uc_email_signature"),
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("Cloud"),
            menu_items=(
                MenuLink(
                    link_name=_("Cloud / XaaS - Anything as a Service"),
                    link_url=reverse("cloud"),
                ),
                MenuLink(
                    link_name=_("Dedicated Server"),
                    link_url=reverse("cloud_dedicated_server"),
                ),
                MenuLink(
                    link_name=_("Virtual Server"),
                    link_url=reverse("cloud_virtual_server"),
                ),
                MenuLink(
                    link_name=_("VDI - Virtual Desktop Infrastructure"),
                    link_url=reverse("cloud_virtual_desktop_infrastructure"),
                ),
                MenuLink(
                    link_name=_("Webhosting"),
                    link_url=reverse("web"),
                ),
                MenuLink(
                    link_name=_("Domains & DNS"),
                    link_url=reverse("web_domains_and_dns"),
                ),
                MenuLink(
                    link_name=_("Fileshare"),
                    link_url=reverse("cloud_fileshare"),
                ),
                MenuLink(
                    link_name=_("Online Backup"),
                    link_url=reverse("cloud_online_backup"),
                ),
                MenuLink(
                    link_name=_("Network Management"),
                    link_url=reverse("cloud_network_management"),
                ),
                MenuLink(
                    link_name=_("Network Security"),
                    link_url=reverse("cloud_network_security"),
                ),
                MenuLink(
                    link_name=_("IoT - Internet of Things"),
                    link_url=reverse("cloud_internet_of_things"),
                ),
                MenuLink(
                    link_name=_("IAM - Identity & Access Management"),
                    link_url=reverse("cloud_identity_and_access_management"),
                ),
            ),
        )
    )

    nav.append(
        MenuGroup(
            menu_name=_("About Us"),
            link_url=reverse("about_us"),
        )
    )

    # nav.append(
    #     MenuGroup(
    #         menu_name=_('Partner'),
    #         link_url=reverse('partner'),
    #     )
    # )

    nav.append(
        MenuGroup(
            menu_name=_("Blog"),
            link_url="/blog/",
        )
    )

    return nav
