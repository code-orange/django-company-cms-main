from random import choice

from django.contrib import messages
from django.utils.translation import gettext as _
from phonenumber_field.phonenumber import PhoneNumber

from django_company_cms_general.django_company_cms_general.forms import (
    GenericContactForm,
)
from django_company_cms_general.django_company_cms_general.func import (
    contact_inquiry_with_image_docs,
)
from django_company_cms_general.django_company_cms_general.models import *
from django_company_cms_main.django_company_cms_main.views import get_nav


def menu_groups(request):
    return {
        "menu_groups": get_nav(request),
    }


def company_details(request):
    return {
        "company_name": CompanyCmsGeneral.objects.get(name="company_name").content,
        "company_address_name_1": CompanyCmsGeneral.objects.get(
            name="company_address_name_1"
        ).content,
        "company_address_name_2": CompanyCmsGeneral.objects.get(
            name="company_address_name_2"
        ).content,
        "company_address_details_1": CompanyCmsGeneral.objects.get(
            name="company_address_details_1"
        ).content,
        "company_address_details_2": CompanyCmsGeneral.objects.get(
            name="company_address_details_2"
        ).content,
        "company_address_phone_1": PhoneNumber.from_string(
            CompanyCmsGeneral.objects.get(name="company_address_phone_1").content,
        ),
        "company_address_phone_2": PhoneNumber.from_string(
            CompanyCmsGeneral.objects.get(name="company_address_phone_2").content,
        ),
        "company_address_email_1": CompanyCmsGeneral.objects.get(
            name="company_address_email_1"
        ).content,
        "company_address_email_2": CompanyCmsGeneral.objects.get(
            name="company_address_email_2"
        ).content,
    }


def more_info_contact(request):
    return {
        "more_info_contact": choice(CompanyCmsMoreInfoContact.objects.all()),
    }


def contact_more_info_form(request):
    # Form
    if request.method == "POST":
        contact_more_info_form = GenericContactForm(request.POST)

        if contact_more_info_form.is_valid():
            request.session["contact_more_info_form_last_request"] = request.POST

            # process the data in form.cleaned_data as required
            message = request.build_absolute_uri()

            if "message" in contact_more_info_form.cleaned_data:
                if contact_more_info_form.cleaned_data["message"] != "":
                    message = contact_more_info_form.cleaned_data["message"]

            try:
                contact_inquiry_with_image_docs(
                    name=contact_more_info_form.cleaned_data["name"],
                    from_email=contact_more_info_form.cleaned_data["from_email"],
                    phone=contact_more_info_form.cleaned_data["phone"].as_international,
                    message=message,
                )
            except:
                messages.error(
                    request, _("Sorry, there was an issue processing the contact data.")
                )
            else:
                messages.success(
                    request,
                    _(
                        "Thank you for your inquiry. We will contact you as soon as possible."
                    ),
                )

    # Fetch last search from session for better UX
    contact_more_info_form_last_request = request.session.get(
        "contact_more_info_form_last_request", None
    )

    if contact_more_info_form_last_request is not None:
        contact_more_info_form = GenericContactForm(contact_more_info_form_last_request)
    else:
        contact_more_info_form = GenericContactForm()

    return {
        "contact_more_info_form": contact_more_info_form,
    }
